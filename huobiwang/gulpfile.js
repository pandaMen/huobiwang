var gulp = require('gulp');
var del = require('del');
var header = require('gulp-header');
var staticHash = require('gulp-resource-hash');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var spritesmith = require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');
var buffer = require('gulp-buffer');
var merge = require('gulp-merge');
var csso = require('gulp-csso');
var chsiRev = require('gulp-chsi-rev');
var less = require('gulp-less');
var path = require('path');
var pkg = require('./package.json');
var banner = ['/**', ' * <%= pkg.name %> - <%= pkg.description %>', ' * @version v<%= pkg.v %>', ' * @link <%= pkg.homepage %>', ' * @author <%= pkg.author.name %>(<%= pkg.author.email %>) <%= pkg.author.url %>', ' */', ''].join('\n');
var jshintConfig = {
    "-W069": false // Literal accessor is better written in dot notation
}

gulp.task('compress-pagejs', function(){
    var r = gulp.src('js/page-src/*.js');

    if (sourcemaps) {
        r = r.pipe(sourcemaps.init());
    }
    r = r.pipe(uglify()).pipe(header(banner, {
        pkg: pkg
    }));
    if (sourcemaps) {
        r = r.pipe(sourcemaps.write('../maps/page/'));
    }
    r = r.pipe(gulp.dest('js/page/'))
    return r;
});

gulp.task('jshint', function(){
    return gulp.src(['js/page-src/*.js']).pipe(jshint(jshintConfig)).pipe(jshint.reporter('default'));
});


gulp.task('sprite', function(){
    var spriteData = gulp.src('css/sprites/*.png').pipe(spritesmith({
        imgName: 'images/sprite.png',
        cssName: 'sprite.css',
		padding: 10
    }));
    // Pipe image stream through image optimizer and onto disk
    // DEV: We must buffer our stream into a Buffer for `imagemin`
    var imgStream = spriteData.img.pipe(buffer()).pipe(imagemin()).pipe(gulp.dest('css/'));
    // Pipe CSS stream through CSS optimizer and onto disk
    var cssStream = spriteData.css.pipe(csso()).pipe(chsiRev()).pipe(gulp.dest('css/'));
    // Return a merged stream to handle both `end` events
    return merge(imgStream, cssStream);
});

gulp.task('less', function(){
    return gulp.src('css/less/*.less')
           .pipe(less({
                paths: [__dirname, 'less', 'includes']
            }))
           .pipe(gulp.dest('css/'));//.pipe(csso())
});

gulp.task('watch', function(){
    gulp.watch(['js/page-src/*.js','css/**/*.less'], ['jshint','compress-pagejs','less']);
});

gulp.task('default', ['resource-hash-css', 'compress-pagejs','less','jshint']);

