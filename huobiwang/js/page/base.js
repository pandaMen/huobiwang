/**
 * huobiwang - 
 * @version v
 * @link 
 * @author () 
 */
function toData(e){if(null==e)return e;var t=[];for(var n in e){var a=n+"="+e[n];t.push(a)}return t.join("&")}function ajax(e){if(e.type=e.type||"get",e.async=e.async||!0,e.data=e.data||null,window.XMLHttpRequest)var t=new XMLHttpRequest;else t=new ActiveXObject("Microsoft.XMLHTTP");if("post"==e.type){t.open(e.type,e.url,e.async),t.setRequestHeader("Content-Type","application/x-www-form-urlencoded");var n=toData(e.data);t.send(n)}else{var a=e.url+"?"+toData(e.data);t.open(e.type,a,e.async),t.send()}t.onreadystatechange=function(){4==t.readyState&&(200<=t.status&&t.status<300||304==t.status?e.success&&e.success(t.responseText):e.error&&e.error(t.status))}}window.getDom={$:function(e){return document.querySelector(e)},$all:function(e){e&&0<e.length&&document.querySelectorAll(e)},getClass:function(e,t){var n=e.className.split(" ");if(e.className){for(var a=0;a<n.length;a++)if(n[a]===t)return;e.className+=" "+t}else e.className=t}},window.localData={set:function(e,t){window.localStorage.setItem(e,t)},get:function(e){if(e)return window.localStorage.getItem(e)},remove:function(e){e&&window.localStorage.removeItem(e)},getSession:function(e){var t=sessionStorage[e];return e&&null!=e?JSON.parse(t).value:null},setSession:function(e,t){sessionStorage.setItem("key","value")},removeSession:function(e){e&&sessionStorage.removeItem(e)}};
//# sourceMappingURL=../maps/page/base.js.map
