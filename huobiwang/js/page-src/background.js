    function Proxy(domains,param){
        this.domains=domains;
        this.nodeList = arrTojson(this.domains);
        this.e = 'function FindProxyForURL(url, host){return "DIRECT";}';
        this.dataUrl = "http://domainlist.site/domains/";
        this.setPacConfig = function (node){
            var node_part = node.replace(/"/g,"'");
            var pacConfig = {};
                pacConfig["mode"] = "pac_script";
                pacConfig["pacScript"] = {};
                pacConfig["pacScript"]["data"] = "function FindProxyForURL(url,host){";
                pacConfig["pacScript"]["data"] += "host=host.toLowerCase();";
                pacConfig["pacScript"]["data"] += "var hostParts = host.split('.');";
                pacConfig["pacScript"]["data"] += "var node=";
                pacConfig["pacScript"]["data"] += node_part;
                pacConfig["pacScript"]["data"] += ";";
                pacConfig["pacScript"]["data"] += "var hostParts = host.split('.');for(var d=hostParts.length-1;d>=0;d--){var part=hostParts[d];node=node[part];if(node == undefined||node==1){break;}} if(node==1) return 'SOCKS5 127.0.0.1:10203'; else return 'system';}";
                chrome.proxy.settings.set({
                    value: pacConfig
                    }
                );    
    }
        this.setProxy = function (){
            var _this = this;
            ajax({
                type:'get',
                url:this.dataUrl,
                async:true,
                data:{
                    tn:param
                },
                success:function(node){
                    _this.setPacConfig(node)
                },
                error:function(err){
                    if(err == 0){
                         _this.setPacConfig(_this.nodeList)
                    }
                   
                    }
                })     
        }

        this.closeProxy = function (){
            var pacConfig = {
                mode: "system",
                pacScript: {
                    data:this.e
                }
            };
            chrome.proxy.settings.set({
                value: pacConfig
            })
           
        }
      
        this.init = function (){ 
            window.localStorage.setItem('info',"false"); 
            var _this = this;
            chrome.extension.onMessage.addListener(function(request, sender, sendResponse){
                if(request.istrue == "checked"){
                    _this.setProxy()
                };
                if(request.istrue == "nochecked"){
                    _this.closeProxy()
                }       
            });
            if(window.localStorage.getItem('info') == "true"){
                _this.setProxy()
                chrome.browserAction.setIcon({
                    path:'img/on.png'
                })
            }else{
                chrome.browserAction.setIcon({
                    path:'img/off.png'
                })
                _this.closeProxy()
            }
        }
    }

    
    function arrTojson(domains){
        var test={};
        domains.forEach(function(item) {
          handle(item,test);
        })
        function handle(item,obj){
          var fromindex = item.length-1;
          var key = item.substr(item.lastIndexOf('.',fromindex)+1);
          var val = item.substr(0,item.lastIndexOf('.',fromindex));
          fromindex = item.lastIndexOf('.',fromindex);
          if(val.lastIndexOf('.',fromindex) != -1){
            obj[key]={};
            handle(val,obj[key]);
          }else{
            if(!obj.hasOwnProperty(key)){
              obj[key] = {};
              obj[key][val] = 1;
            }else{
              obj[key][val] = 1;
            } 
          }
        }   
        var test2= JSON.stringify(test);
        return test2;  
      }


    function toData(obj){
            if (obj == null){
                return obj;
            }
            var arr = [];
            for (var i in obj){
                var str = i+"="+obj[i];
                arr.push(str);
            }
            return arr.join("&");
    }
    function ajax(obj){
            
            obj.type = obj.type || "get";
            obj.async = obj.async || true;          
            obj.data = obj.data || null;          
            var ajax = new XMLHttpRequest();    
            if (obj.type == "post"){
                ajax.open(obj.type,obj.url,obj.async);
                ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                var data = toData(obj.data);
                ajax.send(data);
            }else{
                var url = obj.url+"?"+toData(obj.data);
                ajax.open(obj.type,url,obj.async);
                ajax.send();
            }
            ajax.onreadystatechange = function (){
                if (ajax.readyState == 4){
                        if (ajax.status>=200&&ajax.status<300 || ajax.status==304){
                            if (obj.success){
                                obj.success(ajax.responseText);
                            }
                        }else{
                            if (obj.error){
                                obj.error(ajax.status);
                            }
                        }
                    }
            }    
    }
    
var domains = ["huobipro.com","huobi.com","huobi.pro","hbg.com","google.co.jp"];
var param = "huobi" 
var proxy = new Proxy(domains,param)
    proxy.init();
  
  
    