
//获取元素
window.getDom = {
    $:function(ele){
        // if(ele&&ele.length==0){
          return document.querySelector(ele)
        // }      
    },
    $all:function(ele){
        if(ele && ele.length>0){
            document.querySelectorAll(ele)
        }
    },
    getClass:function(obj, sClass){
        var aClass = obj.className.split(' ');
        if (!obj.className) {
            obj.className = sClass;
            return;
        }
        for (var i = 0; i < aClass.length; i++) {
            if (aClass[i] === sClass) return;
        }
        obj.className += ' ' + sClass;
    }
}


//设置本地存储
window.localData = {
    set:function(key,value){
        window.localStorage.setItem(key,value)
    },
    get:function(key){
        if(key){
        return window.localStorage.getItem(key)
        }     
    },
    remove:function(key){
        if(key){
          window.localStorage.removeItem(key)
        }
        
    },
    getSession:function(key){
        var data = sessionStorage[key]
        if(!key || key==null){
            return null
        }
        return JSON.parse(data).value;
    },
    setSession:function(key,value){
        // var data = {
        //     value = value
        // }
        sessionStorage.setItem("key", "value");
    },
    removeSession:function(key){
        if(key){
            sessionStorage.removeItem(key)
        }
    }
}

function toData(obj){
    if (obj == null){
        return obj;
    }
    var arr = [];
    for (var i in obj){
        var str = i+"="+obj[i];
        arr.push(str);
    }
    return arr.join("&");
}


function ajax(obj){
    //指定提交方式的默认值
    obj.type = obj.type || "get";
    //设置是否异步，默认为true(异步)
    obj.async = obj.async || true;
    //设置数据的默认值
    obj.data = obj.data || null;
    if (window.XMLHttpRequest){
        //非ie
        var ajax = new XMLHttpRequest();
    }else{
        //ie
        var ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //区分get和post
    if (obj.type == "post"){
        ajax.open(obj.type,obj.url,obj.async);
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        var data = toData(obj.data);
        ajax.send(data);
    }else{
        //get  test.php?xx=xx&aa=xx
        var url = obj.url+"?"+toData(obj.data);
        ajax.open(obj.type,url,obj.async);
        ajax.send();
    }
    ajax.onreadystatechange = function (){
        if (ajax.readyState == 4){
                if (ajax.status>=200&&ajax.status<300 || ajax.status==304){
                    if (obj.success){
                        obj.success(ajax.responseText);
                    }
                }else{
                    if (obj.error){
                        obj.error(ajax.status);
                    }
                }
            }
    }    
}